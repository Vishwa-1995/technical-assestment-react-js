// theme constant
export const gridSpacing = 3;
export const drawerWidth = 260;
export const appDrawerWidth = 320;

export const SECRET_KEY = "ASALYA#!00";
export const LANGS = [
    {
        value: 'english',
        label: 'English',
    },
    {
        value: 'sinhala',
        label: 'Sinhala',
    },
    {
        value: 'tamil',
        label: 'Tamil',
    },
];

export const FILE_SIZE = 2097152;
export const FILE_SUPPORTED_FORMATS = "image/jpg,image/jpeg,image/png,application/pdf,";
export const IMAGE_SIZE = 2097152;
export const IMAGE_SUPPORTED_FORMATS = "image/jpg,image/jpeg,image/png,";
export const VIDEO_SIZE = 5242880;
export const VIDEO_SUPPORTED_FORMATS = "video/mp4,video/webm,video/ogg,";
