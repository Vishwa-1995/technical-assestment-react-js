import * as api from "../api";
export const ProfileService = {
    getUserProfile,
};

async function getUserProfile() {
    try {

        const {data} = await api.getProfile();

        return {isSuccess: true, data: data};
    } catch (error) {

        return {isSuccess: false, data: error};
    }
}