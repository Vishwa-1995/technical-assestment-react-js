import * as api from "../api";
import {getProduct, getProducts} from "../api";
export const ProductsService = {
    getProductsList,
    getProductDetails
};

async function getProductsList(currentPage:  number) {
    try {

        const {data} = await api.getProducts(currentPage);

        return {isSuccess: true, data: data.data};
    } catch (error) {

        return {isSuccess: false, data: error};
    }
}

async function getProductDetails(id:  number) {
    try {

        const {data} = await api.getProduct(id);

        return {isSuccess: true, data: data.data};
    } catch (error) {

        return {isSuccess: false, data: error};
    }
}