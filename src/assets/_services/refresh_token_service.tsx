import * as api from "../api";
export const RefreshTokenService = {
    refreshAccessToken,
};

async function refreshAccessToken(token: string) {
    try {

        const {data} = await api.getAccessToken(token);

        return {isSuccess: true, data: data};
    } catch (error) {

        return {isSuccess: false, data: error};
    }
}