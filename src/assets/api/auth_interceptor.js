import axios from "axios";
import {openErrorDialog} from "../../utils/ui-components/pop-ups/ErrorDialog";
import {getLoginSuccess, getLogout, getSate} from "../../redux/actions/actions";
import {RefreshTokenService} from "../_services/refresh_token_service";

let store
let decryptedDefaultData

export const injectStore = _store => {
    store = _store
}


const auth_interceptor = axios.create({
    baseURL:
        process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_DEV
            : process.env.REACT_APP_API_PROD,
});

auth_interceptor.interceptors.request.use(
    (req) => {
        if (store.getState().auth.authData) {
            decryptedDefaultData = getSate(store.getState().auth.authData);
            req.headers[
                "Authorization"
                ] = `Bearer ${decryptedDefaultData.access_token}`;
        }
        return req;
    },
    (error) => {
        return Promise.reject(error);
    }
)

auth_interceptor.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error.response.status === 401) {
            openErrorDialog(error.response.data.status, error.response.data.comment);
            RefreshTokenService.refreshAccessToken(decryptedDefaultData.access_token).then(
                response => {
                    if (response.isSuccess) {
                        store.dispatch(getLoginSuccess(response.data));
                    } else {
                        store.dispatch(getLogout());
                    }
                }
            );
        } else {
            openErrorDialog(error.response.data.status, error.response.data.comment);
        }
    }
);

export default auth_interceptor;