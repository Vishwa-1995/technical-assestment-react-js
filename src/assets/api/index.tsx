import auth_api from "./auth_interceptor";
import products_api from "./products_interceptor";
import axios from "axios";

export const signIn = (formData: any) => {
    let postData = {
        email: formData.username,
        password: formData.password,
    };

    return auth_api.post("/api/v1/auth/login", postData);
};

export const signUp = (formData: { fname: string, lname: string, email: string, mobile: string, username: string, password: string }) => {
    let postData = {
        firstName: formData.fname,
        lastName: formData.lname,
        userName: formData.username,
        password: formData.password,
        contactNumber: formData.mobile,
        email: formData.email
    };

    return auth_api.post("/api/v1/registration/owner-registration", postData);
};

export const getAccessToken = (refreshToken: string) => {

    let postData = {
        refreshToken: refreshToken
    };

    return auth_api.post("/api/v1/auth/profile", postData);
};

export const getProfile = () => {
    return auth_api.get("/api/v1/auth/profile");
};

export const getProducts = (currentPage: number) => {
    let params = {
        page: currentPage,
    };
    return products_api.get("/recommend/items", {params: params});
};

export const getProduct = (id: number) => {
    return products_api.get("/recommend/item"+id,);
};

export const downloadFile = (url: string) => {
    return axios.get(url, {
        responseType: 'blob',
    });
};