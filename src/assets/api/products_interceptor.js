import axios from "axios";
import {openErrorDialog} from "../../utils/ui-components/pop-ups/ErrorDialog";


const products_interceptor = axios.create({
    baseURL:
        process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_PRODUCTS_API_DEV
            : process.env.REACT_APP_PRODUCTS_API_PROD,
});

products_interceptor.interceptors.request.use(
    (req) => {
        return req;
    },
    (error) => {
        return Promise.reject(error);
    }
)

products_interceptor.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        openErrorDialog(error.response.data.status, error.response.data.comment);
    }
);

export default products_interceptor;