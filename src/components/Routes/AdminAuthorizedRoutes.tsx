import {Navigate} from "react-router-dom";
import MainLayout from "../MainLayout";
import Loadable from "./RouteLoad/Loadable";
import {lazy} from "react";

const ProductList = Loadable(lazy(() => import('../Products/products-list')));
const ProductView = Loadable(lazy(() => import('../Products/product view/product-view')));
const UserProfile = Loadable(lazy(() => import('../UserProfile/user-profile')));
const UnauthorizedAccess = Loadable(lazy(() => import('../../utils/ui-components/UnauthorizedAccess')));

const AdminAuthorizedRoutes =
    [
        {
            path: "/",
            element: <MainLayout/>,
            children: [
                {
                    path: '',
                    element: <ProductList/>
                },
                {
                    path: 'all-categories',
                    children: [
                        {
                            path: 'electronics',
                            element: <ProductList/>
                        },
                        {
                            path: 'electronics/view/:id',
                            element: <ProductView/>
                        },
                    ]
                },
                {
                    path: 'user-profile',
                    element: <UserProfile/>
                },
            ]
        },
        {
            path: 'unauthorized',
            element: <UnauthorizedAccess/>
        },
        {
            path: '',
            element: <Navigate to="/all-categories/electronics"/>
        },
        {
            path: 'login',
            element: <Navigate to="/all-categories/electronics"/>
        },
        {
            path: "*",
            element: <Navigate to="/unauthorized"/>
        },
    ];

export default AdminAuthorizedRoutes;