import {useRoutes} from 'react-router-dom';

import UnauthorizedRoutes from './UnauthorizedRoutes';
import AdminAuthorizedRoutes from "./AdminAuthorizedRoutes";
import {useSelector} from "react-redux";
import {getSate} from "../../redux/actions/actions";

// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes() {
    const authState = useSelector((state: any) => state.auth.authData);
    const user = getSate(authState);
    return useRoutes(!(!user?.access_token && !user?.refresh_token) ? AdminAuthorizedRoutes : UnauthorizedRoutes);
}

const ROLES_ROUTES: any = {
    owner: AdminAuthorizedRoutes,
    undefined: UnauthorizedRoutes
};
