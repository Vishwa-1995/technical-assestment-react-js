const icons = {
    electronics: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-propeller" width="20"
                      height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                      strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M12 13m-3 0a3 3 0 1 0 6 0a3 3 0 1 0 -6 0"/>
        <path
            d="M14.167 10.5c.722 -1.538 1.156 -3.043 1.303 -4.514c.22 -1.63 -.762 -2.986 -3.47 -2.986s-3.69 1.357 -3.47 2.986c.147 1.471 .581 2.976 1.303 4.514"/>
        <path
            d="M13.169 16.751c.97 1.395 2.057 2.523 3.257 3.386c1.3 1 2.967 .833 4.321 -1.512c1.354 -2.345 .67 -3.874 -.85 -4.498c-1.348 -.608 -2.868 -.985 -4.562 -1.128"/>
        <path
            d="M8.664 13c-1.693 .143 -3.213 .52 -4.56 1.128c-1.522 .623 -2.206 2.153 -.852 4.498s3.02 2.517 4.321 1.512c1.2 -.863 2.287 -1.991 3.258 -3.386"/>
    </svg>,
    mobile_n_tablets: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-devices"
                           width="20" height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                           strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M13 9a1 1 0 0 1 1 -1h6a1 1 0 0 1 1 1v10a1 1 0 0 1 -1 1h-6a1 1 0 0 1 -1 -1v-10z"/>
        <path d="M18 8v-3a1 1 0 0 0 -1 -1h-13a1 1 0 0 0 -1 1v12a1 1 0 0 0 1 1h9"/>
        <path d="M16 9h2"/>
    </svg>,
    home_appliance: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-home" width="20"
                         height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                         strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M5 12l-2 0l9 -9l9 9l-2 0"/>
        <path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7"/>
        <path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6"/>
    </svg>,
    sports_fitness: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-barbell" width="20"
                         height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                         strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M2 12h1"/>
        <path d="M6 8h-2a1 1 0 0 0 -1 1v6a1 1 0 0 0 1 1h2"/>
        <path d="M6 7v10a1 1 0 0 0 1 1h1a1 1 0 0 0 1 -1v-10a1 1 0 0 0 -1 -1h-1a1 1 0 0 0 -1 1z"/>
        <path d="M9 12h6"/>
        <path d="M15 7v10a1 1 0 0 0 1 1h1a1 1 0 0 0 1 -1v-10a1 1 0 0 0 -1 -1h-1a1 1 0 0 0 -1 1z"/>
        <path d="M18 8h2a1 1 0 0 1 1 1v6a1 1 0 0 1 -1 1h-2"/>
        <path d="M22 12h-1"/>
    </svg>,
    fashion: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-hanger-2" width="20"
                  height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none" strokeLinecap="round"
                  strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M12 9l-7.971 4.428a2 2 0 0 0 -1.029 1.749v.823a2 2 0 0 0 2 2h1"/>
        <path
            d="M18 18h1a2 2 0 0 0 2 -2v-.823a2 2 0 0 0 -1.029 -1.749l-7.971 -4.428c-1.457 -.81 -1.993 -2.333 -2 -4a2 2 0 1 1 4 0"/>
        <path d="M6 16m0 2a2 2 0 0 1 2 -2h8a2 2 0 0 1 2 2v1a2 2 0 0 1 -2 2h-8a2 2 0 0 1 -2 -2z"/>
    </svg>,
    home_living: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-armchair" width="20"
                      height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                      strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M5 11a2 2 0 0 1 2 2v2h10v-2a2 2 0 1 1 4 0v4a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-4a2 2 0 0 1 2 -2z"/>
        <path d="M5 11v-5a3 3 0 0 1 3 -3h8a3 3 0 0 1 3 3v5"/>
        <path d="M6 19v2"/>
        <path d="M18 19v2"/>
    </svg>,
    auto_motive: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-car" width="20"
                      height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                      strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M7 17m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"/>
        <path d="M17 17m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"/>
        <path d="M5 17h-2v-6l2 -5h9l4 5h1a2 2 0 0 1 2 2v4h-2m-4 0h-6m-6 -6h15m-6 0v-5"/>
    </svg>,
    beauty_care: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-perfume" width="20"
                      height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                      strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M10 6v3"/>
        <path d="M14 6v3"/>
        <path d="M5 9m0 2a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v8a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2z"/>
        <path d="M12 15m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"/>
        <path d="M9 3h6v3h-6z"/>
    </svg>,
    toys: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-horse-toy" width="20"
               height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none" strokeLinecap="round"
               strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M3.5 17.5c5.667 4.667 11.333 4.667 17 0"/>
        <path d="M19 18.5l-2 -8.5l1 -2l2 1l1.5 -1.5l-2.5 -4.5c-5.052 .218 -5.99 3.133 -7 6h-6a3 3 0 0 0 -3 3"/>
        <path d="M5 18.5l2 -9.5"/>
        <path d="M8 20l2 -5h4l2 5"/>
    </svg>,
    office_supplies: <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-printer" width="20"
                          height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#2c3e50" fill="none"
                          strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2"/>
        <path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4"/>
        <path d="M7 13m0 2a2 2 0 0 1 2 -2h6a2 2 0 0 1 2 2v4a2 2 0 0 1 -2 2h-6a2 2 0 0 1 -2 -2z"/>
    </svg>
}

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const allCategories = {
    id: 'all-categories',
    title: 'All Categories',
    type: 'group',
    children: [
        {
            id: 'all-categories/electronics',
            title: 'Electronics',
            type: 'item',
            url: '/all-categories/electronics',
            icon: icons.electronics,
            breadcrumbs: false
        },
        {
            id: 'all-categories/mobile-tablets',
            title: 'Mobile & Tablets',
            type: 'item',
            url: '/all-categories/mobile-tablets',
            icon: icons.mobile_n_tablets,
            breadcrumbs: false
        },
        {
            id: 'all-categories/home-appliance',
            title: 'Home Appliance',
            type: 'item',
            url: '/all-categories/home-appliance',
            icon: icons.home_appliance,
            breadcrumbs: false
        },
        {
            id: 'all-categories/sports-fitness',
            title: 'Sports Fitness Outdoor',
            type: 'item',
            url: '/all-categories/sports-fitness',
            icon: icons.sports_fitness,
            breadcrumbs: false
        },
        {
            id: 'all-categories/fashion',
            title: 'Fashion',
            type: 'item',
            url: '/all-categories/fashion',
            icon: icons.fashion,
            breadcrumbs: false
        },
        {
            id: 'all-categories/home-living',
            title: 'Home Living',
            type: 'item',
            url: '/all-categories/home-living',
            icon: icons.home_living,
            breadcrumbs: false
        },
        {
            id: 'all-categories/auto-motive',
            title: 'Auto Motive',
            type: 'item',
            url: '/all-categories/auto-motive',
            icon: icons.auto_motive,
            breadcrumbs: false
        },
        {
            id: 'all-categories/beauty-care',
            title: 'Beauty & Personal Care',
            type: 'item',
            url: '/all-categories/beauty-care',
            icon: icons.beauty_care,
            breadcrumbs: false
        },
        {
            id: 'all-categories/toys',
            title: 'Toys, Kids & Babies',
            type: 'item',
            url: '/all-categories/toys',
            icon: icons.toys,
            breadcrumbs: false
        },
        {
            id: 'all-categories/office-supplies',
            title: 'Stationery & Office Supplies',
            type: 'item',
            url: '/all-categories/office-supplies',
            icon: icons.office_supplies,
            breadcrumbs: false
        }
    ]
};

export default allCategories;
