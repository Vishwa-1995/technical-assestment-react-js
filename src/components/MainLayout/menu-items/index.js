import allCategories from './allCategories';
import other from './other';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
    items: [allCategories, other]
};

export default menuItems;
