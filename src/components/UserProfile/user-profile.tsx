import React, {useEffect, useState} from 'react';
import {gridSpacing, IMAGE_SIZE, IMAGE_SUPPORTED_FORMATS} from "../../store/constants";
import {
    Box,
    Button,
    Divider,
    Grid,
    IconButton,
    InputAdornment, Typography,
} from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import PersonIcon from "@mui/icons-material/Person";
import LockIcon from "@mui/icons-material/Lock";
import MainCard from "../../utils/ui-components/MainCard";
import * as Yup from "yup";
import {Form, Formik} from "formik";
import moment from "moment";
import ItemImageUpload from "../../utils/ui-components/FormsUI/ItemImageUpload/item-image-upload.component";
import TextField from "../../utils/ui-components/FormsUI/TextField";
import LogoutIcon from '@mui/icons-material/Logout';
import {ProductsService} from "../../assets/_services/products_service";
import {ProfileService} from "../../assets/_services/profile_service";
import {_setImage} from "../../utils/utils";
import Spinner from "../../utils/ui-components/Spinner";

function UserProfile() {

    const [passwordShow, setPasswordShow] = React.useState({
        password: "",
        showPassword: false,
        confirmPassword: "",
        showConfirmPassword: false,
    });

    const INITIAL_FORM_STATE = {
        profileImage: {},
        name: "",
        email: "",
        role: "",
        password: "",
        confirmPassword: "",
    };

    const FORM_VALIDATION = Yup.object().shape({
        profileImage: Yup.mixed()
            .nullable()
            .notRequired()
            .test(
                "FILE_SIZE",
                "The size of uploaded files exceeds the maximum limit of 2MB",
                (value: any) => {
                    if (!value)
                        return true;
                    else if (Object?.keys(value)?.length === 0 && value?.constructor === Object)
                        return true;
                    else return (value && value?.size <= IMAGE_SIZE);
                }
            )
            .test(
                "FILE_FORMAT",
                "Uploaded file has unsupported format.",
                (value: any) => {
                    if (!value)
                        return true;
                    else if (Object?.keys(value)?.length === 0 && value?.constructor === Object)
                        return true;
                    else return (value && IMAGE_SUPPORTED_FORMATS.includes(`${value?.type},`));
                }
            ),
        name: Yup.string().required("Please Enter name"),
        email: Yup.string().email('Must be a valid email').max(255).required('Please Enter email'),
        role: Yup.string().required("Please Enter role"),
        password: Yup.string().max(255).required('Password is required').matches(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
            "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
        ),
        passwordConfirmation: Yup.string()
            .oneOf([Yup.ref("password")], "Your passwords do not match.").required("Please retype your password."),
    });

    const [isLoading, setIsLoading] = useState(true);
    const [initialFormState, setInitialFormState]: any = useState(INITIAL_FORM_STATE);

    useEffect(() => {
        fetchProfile();
    }, []);

    const fetchProfile = () => {
        setIsLoading(true);
        ProfileService.getUserProfile().then(
            response => {
                if (response.isSuccess) {
                    // setInitialFormState(response.data);
                    _setImage(
                        response.data.avatar,
                        "profile_image"
                    ).then(((value: any) => {
                        let INITIAL_FORM_STATE_TEMP = {
                            profileImage: value,
                            name: response.data.name,
                            email: response.data.email,
                            role: response.data.role,
                            password: response.data.password,
                            creationAt: response.data.creationAt,
                            updatedAt: response.data.updatedAt,
                        };

                        setInitialFormState(INITIAL_FORM_STATE_TEMP);
                        setIsLoading(false);
                    }));
                } else {
                    setIsLoading(false);
                }
            }
        );
    }

    const handleClickShowPassword = () => {
        setPasswordShow({
            ...passwordShow,
            showPassword: !passwordShow.showPassword,
        });
    };

    const handleClickShowConfirmPassword = () => {
        setPasswordShow({
            ...passwordShow,
            showConfirmPassword: !passwordShow.showConfirmPassword,
        });
    };

    return (
        <Grid container spacing={gridSpacing}>
            {!isLoading ? <Grid item xs={12}>
                <Formik
                    enableReinitialize={true}
                    initialValues={{...initialFormState}}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values, {setSubmitting}) => {
                    }}
                >
                    {({values, dirty, isSubmitting, isValid}) => (
                        <Form>
                            <Grid container spacing={gridSpacing} direction="row">
                                <Grid item md={3} xs={12}>
                                    <MainCard title="User profile">
                                        <ItemImageUpload
                                            accept=".jpg,.png,.jpeg"
                                            label="Profile Image"
                                            maxFileSizeInBytes={IMAGE_SIZE}
                                            name="profileImage"
                                        />
                                    </MainCard>
                                </Grid>
                                <Grid item md={9} xs={12}>
                                    <MainCard title="Profile Details" subtitle={'The information can be edited'}>
                                        <Grid container direction="row" spacing={gridSpacing}>
                                            <Grid item xs={12} md={12}>
                                                <Grid container spacing={gridSpacing}>
                                                    <Grid item>
                                                        <Typography
                                                            sx={{
                                                                fontSize: '0.75rem',
                                                                fontWeight: 600,
                                                                opacity: 0.5
                                                            }}
                                                        >
                                                            Created at:
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography
                                                            sx={{
                                                                fontSize: '0.75rem',
                                                                fontWeight: 600,
                                                            }}
                                                        >
                                                            {moment.utc(new Date(initialFormState?.creationAt)).format("YYYY-MM-DD HH:mm:ss A")}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                                <Grid container spacing={gridSpacing}>
                                                    <Grid item>
                                                        <Typography
                                                            sx={{
                                                                fontSize: '0.75rem',
                                                                fontWeight: 600,
                                                                opacity: 0.5
                                                            }}
                                                        >
                                                            Updated at:
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography
                                                            sx={{
                                                                fontSize: '0.75rem',
                                                                fontWeight: 600,
                                                            }}
                                                        >
                                                            {moment.utc(new Date(initialFormState?.updatedAt)).format("YYYY-MM-DD HH:mm:ss A")}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item md={7} xs={12}>
                                                <TextField
                                                    type={"text"}
                                                    name="name"
                                                    label="Name"
                                                    placeholder="Jane"
                                                />
                                            </Grid>
                                            <Grid item md={5} xs={12}>
                                                <TextField
                                                    type={"text"}
                                                    name="role"
                                                    label="Role"
                                                    placeholder="customer"
                                                />
                                            </Grid>
                                        </Grid>
                                        <Divider
                                            sx={{
                                                marginY: 2,
                                                borderBottomWidth: 1,
                                                borderBottomColor: "var(--primary-color)",
                                            }}
                                        />
                                        <Grid container direction="row" spacing={gridSpacing}>
                                            <Grid item md={6} xs={12}>
                                                <TextField
                                                    type={"text"}
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position="start">
                                                                <PersonIcon/>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    name="email"
                                                    label="Email"
                                                    placeholder="jane@acme.com"
                                                />
                                            </Grid>
                                            <Grid item md={6} xs={12}/>
                                            <Grid item md={6} xs={12}>
                                                <TextField
                                                    type={passwordShow.showPassword ? "text" : "password"}
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position="start">
                                                                <LockIcon/>
                                                            </InputAdornment>
                                                        ),
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <IconButton
                                                                    aria-label="toggle password visibility"
                                                                    onClick={handleClickShowPassword}
                                                                    edge="end"
                                                                >
                                                                    {passwordShow.showPassword ? (
                                                                        <VisibilityOff/>
                                                                    ) : (
                                                                        <Visibility/>
                                                                    )}
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    name="password"
                                                    label="Password"
                                                />
                                            </Grid>
                                            <Grid item md={6} xs={12}>
                                                <TextField
                                                    type={passwordShow.showConfirmPassword ? "text" : "password"}
                                                    InputProps={{
                                                        startAdornment: (
                                                            <InputAdornment position="start">
                                                                <LockIcon/>
                                                            </InputAdornment>
                                                        ),
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <IconButton
                                                                    aria-label="toggle password visibility"
                                                                    onClick={handleClickShowConfirmPassword}
                                                                    edge="end"
                                                                >
                                                                    {passwordShow.showConfirmPassword ? (
                                                                        <VisibilityOff/>
                                                                    ) : (
                                                                        <Visibility/>
                                                                    )}
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    name="passwordConfirmation"
                                                    label="Confirm Password"
                                                />
                                            </Grid>
                                        </Grid>
                                        <Divider
                                            sx={{
                                                marginY: 2,
                                                borderBottomWidth: 1,
                                                borderBottomColor: "var(--primary-color)",
                                            }}
                                        />
                                        <Box
                                            sx={{
                                                display: "flex",
                                                justifyContent: "flex-end",
                                                p: 2,
                                            }}
                                        >
                                            <Button
                                                color="primary"
                                                variant="contained"
                                                className="scf-btn"
                                                type="button"
                                                sx={{mx: 1}}
                                                startIcon={<LogoutIcon/>}
                                            >
                                                Logout
                                            </Button>
                                            <Button
                                                color="primary"
                                                variant="contained"
                                                className="scf-btn"
                                                type="submit"
                                                disabled={!dirty || isSubmitting || !isValid}
                                            >
                                                Save
                                            </Button>
                                        </Box>
                                    </MainCard>
                                </Grid>
                            </Grid>
                        </Form>
                    )}
                </Formik>
            </Grid> : <Box
                style={{
                    width: '100%',
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    minHeight: "40vh",
                }}
            >
                <Spinner/>
            </Box>}
        </Grid>
    );
}

export default UserProfile;