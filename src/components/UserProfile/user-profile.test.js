import UserProfile from "./user-profile";
import React from 'react'
import {render, screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'

test('rendering and submitting a basic Formik form', async () => {
    const handleSubmit = jest.fn()
    render(<UserProfile onSubmit={handleSubmit}/>)
    const user = userEvent.setup()

    await user.type(screen.getByRole("textbox", {name: /name/i}), 'John')
    await user.type(screen.getByRole("textbox", {name: /role/i}), 'customer')
    await user.type(screen.getByRole("textbox", {name: /email/i}), 'john.dee@someemail.com')

    await user.click(screen.getByRole('button', {name: /submit/i}))

    await waitFor(() =>
        expect(handleSubmit).toHaveBeenCalledWith({
            name: 'John',
            role: 'customer',
            email: 'john.dee@someemail.com',
        }),
    )
})