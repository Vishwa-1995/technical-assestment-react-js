import React, {useEffect, useState} from 'react';
import {gridSpacing} from "../../../store/constants";
import {Box, Grid} from "@mui/material";
import {useParams} from "react-router-dom";
import ProductCard from "./product-card";
import ProductServiceCard from "./product-service-card";
import DeliveryDiningIcon from '@mui/icons-material/DeliveryDining';
import PrivacyTipIcon from '@mui/icons-material/PrivacyTip';
import ProductDescriptionCard from "./product-description-card";
import {ProductsService} from "../../../assets/_services/products_service";
import Spinner from "../../../utils/ui-components/Spinner";

function ProductView() {
    const routeParams = useParams();

    const [isLoading, setIsLoading] = useState(true);
    const [product, setProduct]: any = useState({});

    useEffect(() => {
        fetchProduct(Number(routeParams?.id));
    }, []);

    const fetchProduct = (id: number) => {
        setIsLoading(true);
        ProductsService.getProductDetails(id).then(
            response => {
                if (response.isSuccess) {
                    setProduct(response.data.product);
                }
                setIsLoading(false);
            }
        );
    }

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
                {!isLoading ? <ProductCard data={product}></ProductCard> : <></>}
            </Grid>
            <Grid item lg={4} md={4} sm={12} xs={12}>
                {!isLoading ? <><ProductServiceCard icon={<DeliveryDiningIcon/>} title={'Delivery'}
                                                    linkName={'Change Location'}
                                                    linkUrl={'/'} subtitle={'Select city to get info'}/>
                    <ProductServiceCard icon={<PrivacyTipIcon/>} title={'Service'} linkName={'More'}
                                        linkUrl={'/'}/></> : <></>}
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12}>
                {!isLoading ? <ProductDescriptionCard
                    shortDescription={product.shortDescription}
                    code={product.code} productCode={product.product_code}
                    searchUrl={product.search_url}></ProductDescriptionCard> : <></>}
            </Grid>
            {isLoading && <Box
                style={{
                    width: '100%',
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column",
                    minHeight: "40vh",
                }}
            >
                <Spinner/>
            </Box>}
        </Grid>
    );
}

export default ProductView;