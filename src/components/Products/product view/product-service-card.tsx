import React from 'react';
import PropTypes from 'prop-types';
import {gridSpacing} from "../../../store/constants";
import {CardWrapper} from "../card-wrapper";
import {Box, Grid, Link, Typography} from "@mui/material";
import Icon from "@mui/material/Icon";

ProductServiceCard.propTypes = {
    icon: PropTypes.any.isRequired,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    linkName: PropTypes.string,
    linkUrl: PropTypes.string,
};

function ProductServiceCard({icon, title, subtitle, linkName, linkUrl}: any) {

    return (
        <CardWrapper border={false} content={false} sx={{my: 2}}>
            <Box sx={{p: 2.25}}>
                <Grid container spacing={gridSpacing} direction="row">
                    <Grid item>
                        <Icon>{icon}</Icon>
                    </Grid>
                    <Grid item>
                        {title}
                    </Grid>
                    <Grid item xs={8} display='flex' justifyContent='flex-end'>
                        <Link href={linkUrl} underline="none">
                            {linkName}
                        </Link>
                    </Grid>
                </Grid>
                <Grid container spacing={gridSpacing} direction="row">
                    <Grid item>
                        <Typography
                            sx={{
                                fontSize: '0.75rem',
                                fontWeight: 600,
                                opacity: 0.5
                            }}
                        >
                            {subtitle}
                        </Typography>
                    </Grid>
                </Grid>
            </Box>
        </CardWrapper>
    );
}

export default ProductServiceCard;