import React from 'react';
import PropTypes from 'prop-types';
import {Box, Tab, Tabs, Typography} from "@mui/material";
import {CardWrapper} from "../card-wrapper";

ProductDescriptionCard.propTypes = {
    shortDescription: PropTypes.string,
    code: PropTypes.string,
    productCode: PropTypes.string,
    searchUrl: PropTypes.string
};

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function CustomTabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{p: 3}}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

function ProductDescriptionCard({shortDescription, code, productCode, searchUrl}: any) {
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };
    return (
        <CardWrapper border={false} content={false} sx={{my: 2}}>
            <Box sx={{p: 2.25}}>
                <Box sx={{borderBottom: 1, borderColor: 'divider'}}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Description" {...a11yProps(0)} />
                        <Tab label="Code" {...a11yProps(1)} />
                        <Tab label="Product Code" {...a11yProps(2)} />
                        <Tab label="Search Url" {...a11yProps(3)} />
                    </Tabs>
                </Box>
                <CustomTabPanel value={value} index={0}>
                    {shortDescription}
                </CustomTabPanel>
                <CustomTabPanel value={value} index={1}>
                    {code}
                </CustomTabPanel>
                <CustomTabPanel value={value} index={2}>
                    {productCode}
                </CustomTabPanel>
                <CustomTabPanel value={value} index={3}>
                    {searchUrl}
                </CustomTabPanel>
            </Box>
        </CardWrapper>
    );
}

export default ProductDescriptionCard;