import React, {useState} from 'react';
import PropTypes from 'prop-types';
import './carousel-with-thumbs.css';

CarouselWithThumbs.propTypes = {
    images: PropTypes.arrayOf(PropTypes.string).isRequired
};

function CarouselWithThumbs({images}: any) {

    const magnifierHeight = 100;
    const magnifieWidth = 100;
    const zoomLevel = 2.5;
    const width = '100%';
    const height = '100%';
    const [[x, y], setXY] = useState([0, 0]);
    const [[imgWidth, imgHeight], setSize] = useState([0, 0]);
    const [showMagnifier, setShowMagnifier] = useState(false);

    return (
        <section>
            <div className="container">
                <div className="carousel1">
                    {images.map((item: any, index: number) => (
                        <input key={index} type="radio" name="slides" id={`slide-${index}`}/>
                    ))}
                    <ul className="carousel__slides">
                        {images.map((item: any, index: number) => (
                            <li key={index} className="carousel__slide">
                                <figure>
                                    <div
                                        style={{
                                            height: height,
                                            width: width
                                        }}
                                    >
                                        <img
                                            src={item}
                                            style={{height: height, width: width}}
                                            onMouseEnter={(e) => {
                                                // update image size and turn-on magnifier
                                                const elem = e.currentTarget;
                                                const {width, height} = elem.getBoundingClientRect();
                                                setSize([width, height]);
                                                setShowMagnifier(true);
                                            }}
                                            onMouseMove={(e) => {
                                                // update cursor position
                                                const elem = e.currentTarget;
                                                const {top, left} = elem.getBoundingClientRect();

                                                // calculate cursor position on the image
                                                const x = e.pageX - left - window.pageXOffset;
                                                const y = e.pageY - top - window.pageYOffset;
                                                setXY([x, y]);
                                            }}
                                            onMouseLeave={() => {
                                                // close magnifier
                                                setShowMagnifier(false);
                                            }}
                                            alt={"loading failed"}
                                        />

                                        <div
                                            style={{
                                                display: showMagnifier ? "" : "none",
                                                position: "absolute",

                                                // prevent magnifier blocks the mousemove event of img
                                                pointerEvents: "none",
                                                // set size of magnifier
                                                height: `${magnifierHeight}px`,
                                                width: `${magnifieWidth}px`,
                                                // move element center to cursor pos
                                                top: `${y - magnifierHeight / 2}px`,
                                                left: `${x - magnifieWidth / 2}px`,
                                                opacity: "1", // reduce opacity so you can verify position
                                                border: "1px solid lightgray",
                                                backgroundColor: "white",
                                                backgroundImage: `url('${item}')`,
                                                backgroundRepeat: "no-repeat",

                                                //calculate zoomed image size
                                                backgroundSize: `${imgWidth * zoomLevel}px ${
                                                    imgHeight * zoomLevel
                                                }px`,

                                                //calculate position of zoomed image.
                                                backgroundPositionX: `${-x * zoomLevel + magnifieWidth / 2}px`,
                                                backgroundPositionY: `${-y * zoomLevel + magnifierHeight / 2}px`
                                            }}
                                        ></div>
                                    </div>
                                </figure>
                            </li>
                        ))}
                    </ul>
                    <ul className="carousel__thumbnails">
                        {images.map((item: any, index: number) => (
                            <li key={index}>
                                <label htmlFor={`slide-${index}`}>
                                    <img src={item} alt=""/>
                                </label>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        </section>
    );
}

export default CarouselWithThumbs;