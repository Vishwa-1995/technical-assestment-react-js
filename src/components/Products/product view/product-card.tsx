import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Avatar, Box, Button, Divider, Grid, IconButton, TextField, Typography, useTheme} from "@mui/material";
import {currencyFormat, longTextShow, magnify} from "../../../utils/utils";
import ShareIcon from '@mui/icons-material/Share';
import {CardWrapper} from "../card-wrapper";
import {gridSpacing} from "../../../store/constants";
import CarouselWithThumbs from "./carousel-with-thumbs";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';

ProductCard.propTypes = {
    data: PropTypes.object.isRequired,
};

function ProductCard({data}: any) {
    const theme: any = useTheme();

    const [images, setImages] = useState([
        'https://cdnstatic.orelbuy.lk/image-275a3f92-9648-42cc-b95e-c90d421808ff.png',
        'https://orelbuy.lk/_next/image?url=https%3A%2F%2Fcdnstatic.orelbuy.lk%2Fimage-d25a201d-7a52-4d03-b13d-ca5de4d4161c.png%3Ftr%3An-small_thumb&w=1920&q=75',
        'https://orelbuy.lk/_next/image?url=https%3A%2F%2Fcdnstatic.orelbuy.lk%2Fimage-68d7ddc4-014f-4947-b0ad-1f3b0758c489.png%3Ftr%3An-small_thumb&w=1920&q=75',
        'https://orelbuy.lk/_next/image?url=https%3A%2F%2Fcdnstatic.orelbuy.lk%2Fimage-353e3250-1077-473c-a843-dc1ce87d3121.png%3Ftr%3An-small_thumb&w=1920&q=75'
    ]);

    const stock = 3;
    const [quantity, setQuantity] = useState(1);

    useEffect(() => {
        setImages(data.images.map((item: any) => item.url));
    }, []);

    const handleInput = (value: number) => {
        if (value > 0 && value <= stock) {
            setQuantity(value);
        }
    };

    return (
        <CardWrapper border={false} content={false}>
            <Box sx={{p: 2.25}}>
                <Grid container spacing={gridSpacing}>
                    <Grid item lg={5} md={5} sm={12} xs={12}>
                        <CarouselWithThumbs images={images}></CarouselWithThumbs>
                    </Grid>
                    <Grid item lg={7} md={7} sm={12} xs={12}>
                        <Grid container direction="column">
                            <Grid item>
                                <Grid container direction="row">
                                    <Grid item xs={8} md={8}>
                                        <Grid container>
                                            <Typography
                                                sx={{
                                                    fontSize: '0.75rem',
                                                    fontWeight: 600,
                                                    opacity: 0.5
                                                }}
                                            >
                                                Brand:
                                            </Typography>
                                            <Typography
                                                sx={{
                                                    fontSize: '0.75rem',
                                                    fontWeight: 600,
                                                }}
                                            >
                                                XYZ Electric
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={4} md={4} display='flex' justifyContent='flex-end'>
                                        <IconButton color='secondary' size="small">
                                            <ShareIcon/>
                                        </IconButton>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container alignItems="center">
                                    <Grid item>
                                        <Typography
                                            sx={{fontSize: '1.125rem', fontWeight: 550, mr: 1, mt: 1.75, mb: 0.75}}>
                                            {data.name}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid container direction="row">
                                <Grid item>
                                    <Avatar
                                        sx={{
                                            cursor: 'pointer',
                                            opacity: 0.7,
                                            ...theme.typography.smallAvatar,
                                            backgroundColor: 'red',
                                            color: theme.palette.primary.light
                                        }}
                                    >
                                        <ArrowDownwardIcon fontSize="inherit"/>
                                    </Avatar>
                                </Grid>
                                <Box sx={{p: 0.5}}></Box>
                                <Grid item>
                                    <Typography
                                        sx={{
                                            fontSize: '1.2rem',
                                            fontWeight: 550,
                                        }}
                                    >
                                        {currencyFormat(data.discount_price, `${data.price_currency} `, 2)}
                                    </Typography>
                                </Grid>
                                <Box sx={{p: 0.5}}></Box>
                                <Grid item display="flex" alignItems="center">
                                    <Typography
                                        sx={{
                                            textDecoration: 'line-through',
                                            fontSize: '0.8rem',
                                            fontWeight: 550,
                                            opacity: 0.5
                                        }}
                                    >
                                        {currencyFormat(data.price, `${data.price_currency} `, 2)}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Typography
                                    sx={{
                                        paddingY: 2,
                                        color: theme.palette.success.main,
                                        fontSize: '0.8rem',
                                        fontWeight: 600,
                                    }}
                                >
                                    In Stock
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Divider/>
                            </Grid>
                            <Grid item sx={{py: 2}}>
                                <Grid container direction="row">
                                    <Typography
                                        sx={{
                                            fontSize: '0.75rem',
                                            fontWeight: 600,
                                            pr: 0.5
                                        }}
                                    >
                                        Quantity:
                                    </Typography>
                                    <Typography
                                        sx={{
                                            fontSize: '0.75rem',
                                            fontWeight: 600,
                                        }}
                                    >
                                        {`(${stock} item left)`}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container direction="row">
                                    <IconButton size="medium" onClick={() => handleInput(quantity + 1)}>
                                        <AddCircleOutlineIcon/>
                                    </IconButton>
                                    <Grid item xs={3}>
                                        <TextField
                                            type="number"
                                            size="small"
                                            value={quantity}
                                            onChange={(e) => {
                                                handleInput(Number(e.target.value))
                                            }}
                                        />
                                    </Grid>
                                    <IconButton size="medium" onClick={() => handleInput(quantity - 1)}>
                                        <RemoveCircleOutlineIcon/>
                                    </IconButton>
                                </Grid>
                            </Grid>
                            <Grid item sx={{py: 2}}>
                                <Grid container direction="row" spacing={1}>
                                    <Grid item>
                                        <Button variant="contained" disabled>
                                            Added !
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button color="primary" variant="outlined">Add To Cart</Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </CardWrapper>
    );
}

export default ProductCard;