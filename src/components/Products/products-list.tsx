import React, {useEffect, useState} from 'react';
import {gridSpacing} from "../../store/constants";
import {
    Avatar, Box, ButtonBase,
    Grid,
    IconButton,
    Typography, useTheme
} from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import {currencyFormat, longTextShow} from "../../utils/utils";
import {CardWrapper} from "./card-wrapper";
import {ProductsService} from "../../assets/_services/products_service";
import Skeleton from "@mui/material/Skeleton";
import Spinner from "../../utils/ui-components/Spinner";
import {useNavigate} from "react-router-dom";

function ProductsList() {
    const theme: any = useTheme();
    const navigate = useNavigate();

    const [page, setPage] = useState(0);
    const [count, setCount] = useState(0);

    const [isLoading, setIsLoading] = useState(true);
    const [products, setProducts]: any = useState([]);

    const [loadingImg, setLoadingImg] = useState(true);
    const imageLoaded = () => {
        setLoadingImg(false);
    };

    const handleScroll = () => {
        if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight || isLoading) {
            return;
        }
        fetchProductsList(page);
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [page]);

    useEffect(() => {
        fetchProductsList(page);
    }, []);

    const fetchProductsList = (currentPage: number) => {
        setIsLoading(true);
        ProductsService.getProductsList(currentPage).then(
            response => {
                if (response.isSuccess) {
                    setProducts((prevItems: any) => [...prevItems, ...response.data.products]);
                    setPage(prevPage => prevPage + 1);
                    setCount(response.data.total);
                }
                setIsLoading(false);
            }
        );
    }
    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing} direction="row">
                    {products?.map((item: any, index: any) => (
                        <Grid item key={index}>
                            <CardWrapper border={false} content={false} sx={{maxWidth: 250}}>
                                <Box sx={{p: 2.25}}>
                                    <ButtonBase onClick={event => {
                                        navigate(`/all-categories/electronics/view/${index}`)
                                    }}>
                                        <Grid container direction="column">
                                            <Grid item>
                                                <Grid container justifyContent="space-between">
                                                    <Grid item>
                                                        <Avatar
                                                            variant="rounded"
                                                            sx={{
                                                                position: 'absolute',
                                                                ...theme.typography.commonAvatar,
                                                                ...theme.typography.largeAvatar,
                                                                backgroundColor: 'rgba(0, 0, 0, 0.7)',
                                                                mt: 1,
                                                                px: 1,
                                                                width: '30%'
                                                            }}
                                                        >
                                                            <Typography variant="h5" gutterBottom
                                                                        sx={{color: theme.palette.primary.light}}>{item.discount}%
                                                                off</Typography>
                                                        </Avatar>
                                                        {!loadingImg ? <img
                                                            src={item.images[0]}
                                                            alt=''
                                                            loading="lazy"
                                                            style={{
                                                                height: "100%",
                                                                width: "100%",
                                                                borderRadius: "15px 0",
                                                            }}
                                                        /> : <Box>
                                                            <img
                                                                src={item.images[0]}
                                                                alt=''
                                                                onLoad={imageLoaded}
                                                                loading="lazy"
                                                                style={{
                                                                    height: "0px",
                                                                    width: "100%",
                                                                    borderRadius: "15px 0",
                                                                }}
                                                            />
                                                            <Skeleton
                                                                animation="wave"
                                                                variant="rectangular"
                                                                width="200px"
                                                                height="200px"
                                                            />
                                                        </Box>}
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item>
                                                <Grid container>
                                                    <Grid item>
                                                        <Typography
                                                            sx={{
                                                                fontSize: '1.125rem',
                                                                fontWeight: 500,
                                                                mr: 1,
                                                                mt: 1.75,
                                                                mb: 0.75
                                                            }}>
                                                            {longTextShow(item.name, 22)}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item>
                                                <Grid container>
                                                    <Typography
                                                        sx={{
                                                            textDecoration: 'line-through',
                                                            fontSize: '1rem',
                                                            fontWeight: 550,
                                                            opacity: 0.5
                                                        }}
                                                    >
                                                        {currencyFormat(item.price, `${item.price_currency} `, 2)}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                            <Grid item>
                                                <Grid container>
                                                    <Typography
                                                        color='primary'
                                                        sx={{
                                                            fontSize: '1rem',
                                                            fontWeight: 550,
                                                        }}
                                                    >
                                                        {currencyFormat(item.discount_price, `${item.price_currency} `, 2)}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </ButtonBase>
                                    <Grid container>
                                        <Grid item xs={12} display='flex' justifyContent='flex-end'>
                                            <IconButton color='primary' size="large">
                                                <AddCircleOutlineIcon fontSize="inherit"/>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </CardWrapper>
                        </Grid>
                    ))}
                </Grid>
                {isLoading && <Box
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                    }}
                >
                    <Spinner/>
                </Box>}
            </Grid>
        </Grid>
    );
}

export default ProductsList;