import React, {useEffect, useRef, useState} from "react";
import {FormField,} from "./item-image-upload.styles";
import CancelIcon from '@mui/icons-material/Cancel';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import {useField, useFormikContext} from "formik";
import {Avatar, Badge, Box, Button, Divider, Grid, Typography} from "@mui/material";
import Skeleton from "@mui/material/Skeleton";
import {gridSpacing, IMAGE_SIZE} from "../../../../store/constants";

const DEFAULT_MAX_FILE_SIZE_IN_BYTES = IMAGE_SIZE;

const ItemImageUpload = ({
                             label,
                             name,
                             maxFileSizeInBytes = DEFAULT_MAX_FILE_SIZE_IN_BYTES,
                             ...otherProps
                         }) => {
    const fileInputField = useRef(null);
    // const [files, setFiles] = useState({});
    const {setFieldValue} = useFormikContext();
    const [field, meta] = useField(name);
    const [url, setUrl] = useState(null);

    const [loadingImg, setLoadingImg] = useState(true);
    const imageLoaded = () => {
        setLoadingImg(false);
    };


    const handleUploadBtnClick = () => {
        fileInputField.current.click();
    };

    const handleNewFileUpload = (e) => {
        const {files: newFiles} = e.target;
        if (newFiles.length) {
            // console.log("file name", name);
            setFieldValue(name, newFiles[0]);
            setUrl(URL.createObjectURL(newFiles[0]));
        }
    };

    const removeFile = () => {
        delete field.value[0];
        setFieldValue(name, {...field.value});
        setUrl(null);
    };

    useEffect(() => {
        if (field.value && typeof field.value.name == 'string') {
            setUrl(URL.createObjectURL(field.value));
        }
    }, [])

    return (
        <Grid container spacing={gridSpacing} sx={{mb: 2}}>
            <Grid item md={12} xs={12}>
                <Typography variant="body2" color="textSecondary">
                    {label}
                </Typography>
                <Box
                    sx={{
                        mt: 2,
                        alignItems: "center",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    {url && <Badge
                        overlap="circular"
                        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                        badgeContent={
                            <CancelIcon onClick={() => removeFile()}/>
                        }
                        sx={{cursor: 'pointer', color: 'var(--primary-color)', mb: -2, width: 135,}}
                    />
                    }
                    {!loadingImg ? <Avatar
                        src={url}
                        sx={{
                            height: 130,
                            mb: 2,
                            width: 130,
                            border: '5px solid var(--primary-color-30)'
                        }}
                    /> : <Box>
                        <Avatar
                            src={url || 'https://icon-library.com/images/stock-inventory-icon/stock-inventory-icon-15.jpg'}
                            onLoad={imageLoaded}
                            alt={''}
                            loading="lazy"
                            style={{
                                height: "0px",
                                width: "100%",
                                borderRadius: "15px 0",
                            }}
                        />
                        <Skeleton
                            animation="wave"
                            variant="rectangular"
                            width="100%"
                            height="200px"
                        />
                    </Box>
                    }

                    <Typography color="secondary" variant="caption" style={{textAlign: 'center'}}>
                        Select an image file on your computer (2MB max)
                    </Typography>
                </Box>
                <Divider sx={{m: 2, borderBottomWidth: 1, borderBottomColor: 'var(--primary-color)'}}/>
                <Button color="primary" fullWidth variant="outlined" type="button" onClick={handleUploadBtnClick}
                        disabled={otherProps.disabled} startIcon={<CloudUploadIcon/>}>
                    Upload picture
                </Button>
                <FormField
                    type="file"
                    ref={fileInputField}
                    onChange={handleNewFileUpload}
                    title=""
                    value=""
                    {...otherProps}
                />
            </Grid>
            <Grid item md={12} xs={12}>
                {meta && meta.error ? <Typography variant="caption" color='error'>{meta.error}</Typography> : null}
            </Grid>
        </Grid>
    );
};

export default ItemImageUpload;